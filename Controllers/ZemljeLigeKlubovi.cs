﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using spajanjesabazom.Models;
using Microsoft.AspNetCore.Mvc;

namespace spajanjesabazom.Controllers
{



    [Route("ZemljeLigeKlubovi/[controller]")]
    public class ZemljeLigeKluboviController : ControllerBase
    { 

        [HttpGet("/api/zemljeligeklubovi/{IDKategorija}")]
    
        public List<ZemljeLigeKlubovi> ZMKItems(int IDKategorija)
        {
            List<ZemljeLigeKlubovi> ZMKList = new List<ZemljeLigeKlubovi>();
            ZemljeLigeKlubovi ZMK = null;

            using (var con = db.Gdb())
            {

                SqlCommand cmd = new SqlCommand("spGetZemljeLigeKlubovii", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdKategorija", IDKategorija);
                con.Open();
                SqlDataReader rd = cmd.ExecuteReader();



                while (rd.Read())
                {
                    ZMK = new ZemljeLigeKlubovi();
                    ZMK.IDKategorija = Convert.ToInt32(rd["IDKategorija"]);
                    ZMK.Kategorija_NazivPrevod = Convert.ToString(rd["KategorijaNaziv"]);
                    ZMK.Turnir_NazivPrevod = Convert.ToString(rd["TurnirNaziv"]);
                    ZMK.Tim_NazivPrevod = Convert.ToString(rd["TimNaziv"]);
                    ZMKList.Add(ZMK);
                }
            }
            return ZMKList;
        }

    }
}




