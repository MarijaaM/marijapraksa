﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace spajanjesabazom.Models
{
    public class ZemljeLigeKlubovi
    {
        public int IDKategorija { get; set; }
        
        public string Kategorija_NazivPrevod { get; set; }
        public string Turnir_NazivPrevod { get; set; }
        public string Tim_NazivPrevod { get; set; }

    }
}
