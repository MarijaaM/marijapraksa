﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;



namespace spajanjesabazom.Models
{
    
    
        public class db
        {
            SqlConnection con;
            public static SqlConnection Gdb()
            {
                IConfigurationRoot configuration = db.GetConfiguration();
            SqlConnection con = new SqlConnection(configuration.GetSection("Data").GetSection("ConnectionStrings").Value);
            return con;
            }
            public static IConfigurationRoot GetConfiguration()
            {
                var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                return builder.Build();
            }
        }
}
